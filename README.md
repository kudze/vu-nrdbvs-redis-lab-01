# Small redis ORM demo.

A task was to create three entities in Redis, from which one must have compositional primary key. And create transaction/trancations that utilize multiple entities.

## Requirements

* PHP 8
* Redis
* Composer

## Running

1. Clone the repostiory.
2. Run `composer install` to install needed dependencies.
3. Run `php client.php` and have fun!