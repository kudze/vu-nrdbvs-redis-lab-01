<?php

namespace Kudze\NrbdvsRedis\Services;

class Hasher
{

    public function hash(string $password): string
    {
        return password_hash($password, PASSWORD_ARGON2I);
    }

    public function verify(string $hash, string $password): bool
    {
        return password_verify($password, $hash);
    }

}