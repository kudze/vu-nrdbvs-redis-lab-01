<?php

namespace Kudze\NrbdvsRedis\Services;

use \Redis;

class RedisConnector
{
    private Redis $redis;

    public function __construct(string $ip = '127.0.0.1', int $port = 6379)
    {
        $this->redis = $this->connect($ip, $port);
    }

    private function connect(string $ip, int $port) : Redis
    {
        $redis = new Redis();
        $redis->connect($ip, $port);
        return $redis;
    }

    /**
     * @return Redis
     */
    public function getRedis(): Redis
    {
        return $this->redis;
    }
}