<?php

namespace Kudze\NrbdvsRedis\Services;

use Kudze\NrbdvsRedis\Models\User;
use Kudze\NrbdvsRedis\Repositories\UserRepository;

class Session
{
    private ?User $user = null;
    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function setLoggedInUser(?User $user)
    {
        $this->user = $user;
    }

    public function getLoggedInUser(): ?User
    {
        return $this->user;
    }

    public function isLoggedInToAnyUser(): bool
    {
        return $this->getLoggedInUser() !== null;
    }

    public function refreshUser(): ?User
    {
        if(!$this->isLoggedInToAnyUser())
            return null;

        $this->user = $this->userRepository->find(User::class, $this->user->getData());

        return $this->getLoggedInUser();
    }
}