<?php

namespace Kudze\NrbdvsRedis\Services;

use Kudze\NrbdvsRedis\Models\Bill;
use Kudze\NrbdvsRedis\Models\User;
use LucidFrame\Console\ConsoleTable;

class TablePrinter
{
    public function printCompanies(array $companies)
    {
        $table = new ConsoleTable();
        $table->addHeader('id')
            ->addHeader('Title');
        foreach ($companies as $company) {
            $table->addRow()
                ->addColumn($company->getId())
                ->addColumn($company->getTitle());
        }
        $table->setPadding(2);
        $table->display();
    }

    public function printUsers(array $users)
    {
        $table = new ConsoleTable();
        $table
            ->addHeader('Email')
            ->addHeader('Password')
            ->addHeader('First Name')
            ->addHeader('Last Name')
            ->addHeader('Balance');
        foreach ($users as $user) {
            /** @var User $user */
            $table->addRow()
                ->addColumn($user->getEmail())
                ->addColumn($user->getPassword())
                ->addColumn($user->getFirstName())
                ->addColumn($user->getLastName())
                ->addColumn($user->getBalance());
        }

        $table->setPadding(2);
        $table->display();
    }

    public function printBills(array $bills)
    {
        $table = new ConsoleTable();
        $table
            ->addHeader('Company Id')
            ->addHeader('User Email')
            ->addHeader('Unpaid ammount');
        foreach ($bills as $bill) {
            /** @var Bill $bill */
            $table->addRow()
                ->addColumn($bill->getCompanyId())
                ->addColumn($bill->getUserEmail())
                ->addColumn($bill->getAmount());
        }

        $table->setPadding(2);
        $table->display();
    }


}