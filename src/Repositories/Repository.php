<?php

namespace Kudze\NrbdvsRedis\Repositories;

use \InvalidArgumentException;
use Kudze\NrbdvsRedis\Exceptions\ModelAlreadyExists;
use Kudze\NrbdvsRedis\Exceptions\ModelAutoIncrementOffException;
use Kudze\NrbdvsRedis\Exceptions\ModelDataUnparseableException;
use Kudze\NrbdvsRedis\Exceptions\ModelKeyNotDefinedException;
use Kudze\NrbdvsRedis\Exceptions\ModelNotAutoIncrementable;
use Kudze\NrbdvsRedis\Exceptions\ModelNotFoundException;
use Kudze\NrbdvsRedis\Models\AbstractModel;
use Kudze\NrbdvsRedis\Services\Logger;
use Kudze\NrbdvsRedis\Services\RedisConnector;

class Repository
{
    private RedisConnector $connector;
    private Logger $logger;

    public function __construct(RedisConnector $connector, Logger $logger)
    {
        $this->connector = $connector;
        $this->logger = $logger;
    }

    /**
     * Atomic.
     * Returns next key for model doesn't increase counter.
     *
     * @throws ModelNotAutoIncrementable
     */
    public function getAutoIncrement(AbstractModel|string $model)
    {
        $key = $model::getAutoIncrementKey();
        $redis = $this->connector->getRedis();
        $result = $redis->get($key);

        if ($result === FALSE)
            return 0;

        return $result;
    }

    /**
     * Finds single model in redis.
     * Is atomic.
     *
     * @param string $model_class
     * @param array $key - allows to lookup only by primary keys, all must be defined if multiple.
     * @return AbstractModel
     * @throws ModelDataUnparseableException
     * @throws ModelNotFoundException
     * @throws ModelKeyNotDefinedException
     */
    public function find(string $model_class, array $key): AbstractModel
    {
        $redis = $this->connector->getRedis();
        $redisKey = $model_class::getModelKeyFromData($key);

        $data = $redis->get($redisKey);

        if ($data === false)
            throw new ModelNotFoundException($model_class::getTable(), $redisKey);

        return $this->parseModelData($model_class, $data);
    }

    /**
     * Not atomic, if models deleted between key and value check we return false.
     *
     * @throws ModelDataUnparseableException
     * @throws ModelNotFoundException
     */
    public function findAll(string $model_class): array
    {
        $redis = $this->connector->getRedis();
        $dataKeys = $redis->keys($model_class::getDataKeysPattern());

        return $this->findModelsByRedisKeyAtomically($model_class, $dataKeys, true);
    }

    public function findModelsByRedisKeyAtomically(string $model_class, array $redis_keys, bool $skipMissed = false): array
    {
        $redis = $this->connector->getRedis();

        //Transaction here will guarantee that all entries will be read at one time.
        $redis->multi();
        foreach ($redis_keys as $key)
            $redis->get($key);
        $data = $redis->exec();

        $result = [];
        foreach ($data as $redisRow) {
            if($data === false && $skipMissed)
                continue;

            $result[] = $this->parseModelData($model_class, $redisRow);
        }

        return $result;
    }

    /**
     * Is atomic. (Multi transactions with watch (optimistic locking) used)
     *
     * @throws ModelKeyNotDefinedException
     * @throws ModelAlreadyExists
     * @throws ModelAutoIncrementOffException
     */
    public function insert(AbstractModel $model): void
    {
        try {
            $key = $model->getKey();
        } catch (ModelKeyNotDefinedException $exception) {

            //If key isn't defined we're going to try to use auto increments inside transaction.
            try {
                $this->insertAutoIncrement($model, $exception->getKey());
                return;
            } catch (ModelNotAutoIncrementable) {
                throw $exception;
            }

        }

        try {

            //Otherwise, if we support increment keys we need transaction.
            $this->insertDefinedButUpdateAutoIncrement($model, $key);

        } catch (ModelNotAutoIncrementable) {

            //If we don't support increment keys then this operation is really simple, and we don't need any transaction.
            $res = $this->updateRowDataIfNotExitsWhenKeyKnown($model, $key);
            if ($res === false)
                throw new ModelAlreadyExists($model::getTable(), $key);

        }
    }

    /**
     *
     *
     * @param AbstractModel $model
     * @throws ModelKeyNotDefinedException
     */
    public function update(AbstractModel $model): void
    {
        $this->updateRowData($model);
    }

    public function delete(AbstractModel $model)
    {
        if (!$this->doesTableRowExist($model))
            throw new ModelNotFoundException($model->getTable(), $model->getKey());

        $this->deleteRowData($model);
    }

    /**
     * @throws ModelNotAutoIncrementable
     */
    protected function insertDefinedButUpdateAutoIncrement(AbstractModel $model, string $key)
    {
        $incKey = $model::getAutoIncrementKey();
        $currVal = $model->getCurrentIDValue();

        while (true) {
            $redis = $this->connector->getRedis();

            $redis->watch($incKey);
            $currIncVal = $redis->get($incKey);
            if ($currIncVal === FALSE)
                $currIncVal = 0;

            if ($currIncVal > $currVal) {
                $redis->unwatch();

                //If increment value is higher than current model id, we dont need to update it, so it means we won't need any transactions.
                if (!$this->updateRowDataIfNotExitsWhenKeyKnown($model, $key))
                    throw new ModelAlreadyExists($model::getTable(), $key);

                return;
            }

            $redis->multi();
            $this->updateRowDataIfNotExitsWhenKeyKnown($model, $key);
            $redis->set($incKey, $currVal + 1);
            $result = $redis->exec();

            if ($result !== FALSE && $result !== null)
                return;

            $this->logger->printdebugln("Update auto increment transaction failed, retrying...");
        }
    }

    /**
     * @throws ModelNotAutoIncrementable
     * @throws ModelAutoIncrementOffException
     */
    protected function insertAutoIncrement(AbstractModel $model, string $keyCol)
    {
        $incKey = $model::getAutoIncrementKey();
        $redis = $this->connector->getRedis();

        while (true) {
            $redis->watch($incKey);

            $value = $redis->get($incKey);
            if ($value === FALSE)
                $value = 0;
            $model->setDataCol($keyCol, $value);
            $key = $model->getKey();

            $redis->multi();
            $this->updateRowDataIfNotExitsWhenKeyKnown($model, $key);
            $redis->incr($incKey);
            $res = $redis->exec();

            if ($res !== FALSE && $res !== null) {
                if ($res[0] === false) {
                    throw new ModelAutoIncrementOffException($model::getTable());
                }

                return;
            }
        }
    }

    protected function doesTableRowExist(AbstractModel $model): bool
    {
        return $this->connector->getRedis()->exists($model->getKey());
    }

    protected function parseModelData(string $model_class, mixed $data): AbstractModel
    {
        if ($data === false)
            throw new ModelNotFoundException($model_class::getTable());

        $data = unserialize($data);
        if ($data === null || $data === false)
            throw new ModelDataUnparseableException($model_class::getTable());

        $model = new $model_class();
        if (!($model instanceof AbstractModel))
            throw new InvalidArgumentException('argument $model_class should be class name of model!');

        $model->setData($data);
        return $model;
    }

    /**
     * atomic
     *
     * @throws ModelKeyNotDefinedException
     */
    protected function updateRowData(AbstractModel $model, ?string $key = null): bool
    {
        if ($key === null)
            $key = $model->getKey();

        $res = $this->connector->getRedis()->set(
            $model->getKey(),
            serialize($model->getData())
        );

        //if we in transaction we get redis instance.
        if ($res instanceof \Redis)
            return true;

        return $res;
    }

    /**
     * atomic
     *
     * @throws ModelKeyNotDefinedException
     */
    protected function updateRowDataIfNotExists(AbstractModel $model): bool
    {
        $key = $model->getKey();

        $this->updateRowDataIfNotExitsWhenKeyKnown($model, $key);
    }

    /**
     * @param AbstractModel $model
     * @param string $key
     * @return bool
     */
    protected function updateRowDataIfNotExitsWhenKeyKnown(AbstractModel $model, string $key): bool
    {
        $res = $this->connector->getRedis()->setnx(
            $key,
            serialize($model->getData())
        );

        if ($res instanceof \Redis)
            return true;

        return $res;
    }

    /**
     * atomic
     *
     * @throws ModelKeyNotDefinedException
     */
    protected function deleteRowData(AbstractModel $model, ?string $key = null): bool
    {
        if ($key === null)
            $key = $model->getKey();

        $res = $this->connector->getRedis()->del($key);

        if ($res instanceof \Redis)
            return true;

        return $res === 1;
    }

    /**
     * @return RedisConnector
     */
    public function getConnector(): RedisConnector
    {
        return $this->connector;
    }

    /**
     * @return Logger
     */
    public function getLogger(): Logger
    {
        return $this->logger;
    }
}