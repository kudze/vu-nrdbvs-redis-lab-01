<?php

namespace Kudze\NrbdvsRedis\Repositories;

use Kudze\NrbdvsRedis\Exceptions\ModelException;
use Kudze\NrbdvsRedis\Exceptions\UserPayUserNoLongerExists;
use Kudze\NrbdvsRedis\Models\Bill;
use Kudze\NrbdvsRedis\Models\User;

class UserRepository extends Repository
{

    public function getUserBills(User $user): array
    {
        $redis = $this->getConnector()->getRedis();
        $dataKeys = $redis->keys($user->getMyBillsPattern());

        return $this->findModelsByRedisKeyAtomically(Bill::class, $dataKeys, true);
    }

    /**
     * Atomicity guaranteed.
     *
     * @throws \Kudze\NrbdvsRedis\Exceptions\ModelDataUnparseableException
     * @throws UserPayUserNoLongerExists
     * @throws \Kudze\NrbdvsRedis\Exceptions\ModelNotFoundException
     * @throws \Kudze\NrbdvsRedis\Exceptions\ModelKeyNotDefinedException
     */
    public function payUsersBills(User $user, float $amount): void
    {
        $redis = $this->getConnector()->getRedis();
        $userKey = $user->getKey();

        while (true) {
            $redis->watch($userKey);

            $userData = $redis->get($userKey);
            if ($userData === false) {
                $redis->unwatch();

                throw new UserPayUserNoLongerExists(User::getTable(), null, "User which wanted to pay bills no longer exists!");
            }

            /** @var User $userCacheModel */
            $userCacheModel = $this->parseModelData(User::class, $userData);
            $userCacheAmount = $userCacheModel->getBalance();
            $userCacheAmount += $amount;

            if ($userCacheAmount <= 0) {
                //if user amt is still negative then we just update model.
                $userCacheModel->setBalance($userCacheAmount);

                $redis->multi();
                $this->update($userCacheModel);
                $res = $redis->exec();

                if ($res !== null && $res !== false)
                    return;

                else
                    continue;
            }

            //Otherwise, we will need to update bills too.
            //So lets read them and run transaction.
            $billsKeys = $redis->keys($userCacheModel->getMyBillsPattern());

            $billsCache = [];

            $rerun = false;
            foreach ($billsKeys as $key) {
                $redis->watch($key);
                $billCacheData = $redis->get($key);
                if($billCacheData === false) //if key no longer exists it means that bill has been removed from other client (maybe it added new bills too?). let's rerun this task.
                {
                    $redis->unwatch();

                    $rerun = true;
                    break;
                }

                /** @var Bill $billCacheModel */
                $billCacheModel = $this->parseModelData(Bill::class, $billCacheData);

                $oldAmount = $billCacheModel->getAmount();
                $newAmount = max(0, $oldAmount - $userCacheAmount);
                $billCacheModel->setAmount($newAmount);
                $billsCache[] = $billCacheModel;

                $userCacheAmount -= ($oldAmount - $newAmount);
                if($userCacheAmount <= 0)
                {
                    break;
                }
            }
            if($rerun)
                continue;

            $redis->multi();

            $userCacheModel->setBalance($userCacheAmount);
            $this->update($userCacheModel);
            foreach($billsCache as $bill)
                $this->update($bill);

            $res = $redis->exec();
            if($res !== false && $res !== null)
                return;
        }
    }

}