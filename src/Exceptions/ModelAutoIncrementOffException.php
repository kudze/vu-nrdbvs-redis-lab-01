<?php

namespace Kudze\NrbdvsRedis\Exceptions;

class ModelAutoIncrementOffException extends ModelException
{

    public function __construct(string $table)
    {
        parent::__construct(
            $table,
            'auto-increment',
            "$table table's auto increment is off!"
        );
    }

}