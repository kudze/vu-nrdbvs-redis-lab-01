<?php

namespace Kudze\NrbdvsRedis\Exceptions;

class ModelAlreadyExists extends ModelException
{
    public function __construct(string $table, string $key)
    {
        parent::__construct(
            $table,
            $key,
            "Model from table \"$table\" already exists!",
            500
        );
    }
}