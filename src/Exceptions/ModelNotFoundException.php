<?php

namespace Kudze\NrbdvsRedis\Exceptions;

class ModelNotFoundException extends ModelException
{
    public function __construct(string $table, string $key = 'unknown')
    {
        parent::__construct(
            $table,
            $key,
            "Model from table \"$table\" was not found!",
            404
        );
    }
}