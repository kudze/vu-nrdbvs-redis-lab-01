<?php

namespace Kudze\NrbdvsRedis\Exceptions;

class ModelNotAutoIncrementable extends ModelException
{
    public function __construct(string $table)
    {
        parent::__construct(
            $table,
            'auto increment',
            "Model from table \"$table\" doesnt support incrementable keys! (You must define primary keys before insert!)",
            500
        );
    }
}