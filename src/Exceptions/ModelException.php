<?php

namespace Kudze\NrbdvsRedis\Exceptions;

class ModelException extends \Exception
{
    protected string $table;
    protected ?string $key;

    public function __construct(string $table, ?string $key = null, string $message = "", int $code = 0)
    {
        parent::__construct(
            $message,
            $code
        );

        $this->table = $table;
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getTable(): string
    {
        return $this->table;
    }

    /**
     * @return ?string
     */
    public function getKey(): ?string
    {
        return $this->key;
    }
}