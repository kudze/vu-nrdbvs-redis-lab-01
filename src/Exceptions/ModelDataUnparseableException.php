<?php

namespace Kudze\NrbdvsRedis\Exceptions;

class ModelDataUnparseableException extends ModelException
{
    public function __construct(string $table, string $key = 'unknown')
    {
        parent::__construct(
            $table,
            $key,
            "We failed to parse model's from table \"$table\" data!",
            500
        );
    }
}