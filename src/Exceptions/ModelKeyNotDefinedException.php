<?php

namespace Kudze\NrbdvsRedis\Exceptions;

class ModelKeyNotDefinedException extends ModelException
{
    public function __construct(string $table, string $key)
    {
        parent::__construct(
            $table,
            $key,
            "Model from table \"$table\" has undefined key \"$key\"!",
            500
        );
    }
}