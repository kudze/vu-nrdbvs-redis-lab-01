<?php

namespace Kudze\NrbdvsRedis\Commands;

use DI\Container;
use Kudze\NrbdvsRedis\Exceptions\UserPayUserNoLongerExists;
use Kudze\NrbdvsRedis\Models\Bill;
use Kudze\NrbdvsRedis\Models\User;
use Kudze\NrbdvsRedis\Repositories\Repository;
use Kudze\NrbdvsRedis\Repositories\UserRepository;
use Kudze\NrbdvsRedis\Services\Session;
use Kudze\NrbdvsRedis\Services\TablePrinter;

class PayMyBillsCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'pay',
            'Pays your bills',
            $container
        );
    }

    public function run(string $params)
    {
        $logger = $this->getLogger();
        $session = $this->getContainer()->get(Session::class);

        if(!$session->isLoggedInToAnyUser())
        {
            $logger->println("You arent logged in to any user!");
            return;
        }

        $tablePrinter = $this->getContainer()->get(TablePrinter::class);
        $repository = $this->getContainer()->get(UserRepository::class);

        $logger->println("Current user:");
        $user = $session->getLoggedInUser();
        $tablePrinter->printUsers([$user]);

        $logger->println("Users bills:");
        $bills = $repository->getUserBills($user);
        $tablePrinter->printBills($bills);

        $amount = $this->getInputter()->askForPositiveFloat("Enter ammount, which you will pay:");

        $logger->println("Performing transaction...");
        try {
            $repository->payUsersBills($user, $amount);
        } catch(UserPayUserNoLongerExists) {
            $logger->println("Seems like your user has been removed?");
            $session->setLoggedInUser(null);
            return;
        }
        $logger->println("Transaction has executed!");

        $logger->println("Current user:");
        $user = $session->refreshUser();
        $tablePrinter->printUsers([$user]);

        $logger->println("Users bills:");
        $bills = $repository->getUserBills($user);
        $tablePrinter->printBills($bills);
    }
}