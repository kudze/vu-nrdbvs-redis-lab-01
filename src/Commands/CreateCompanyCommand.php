<?php

namespace Kudze\NrbdvsRedis\Commands;

use DI\Container;
use Kudze\NrbdvsRedis\Models\Company;
use Kudze\NrbdvsRedis\Models\User;
use Kudze\NrbdvsRedis\Repositories\Repository;
use Kudze\NrbdvsRedis\Services\Hasher;
use Kudze\NrbdvsRedis\Services\Inputter;
use Kudze\NrbdvsRedis\Services\Logger;

class CreateCompanyCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'ccompany',
            'Creates a company',
            $container
        );
    }

    public function run(string $params)
    {
        $repository = $this->getContainer()->get(Repository::class);
        $logger = $this->getLogger();
        $inputter = $this->getInputter();

        $title = $inputter->askForAnyTextInLength(3, 256, "Enter company title:");

        $company = new Company();
        $company->setTitle($title);
        $repository->insert($company);

        $logger->println("Company has been successfully created!");
    }
}