<?php

namespace Kudze\NrbdvsRedis\Commands;

use DI\Container;
use Kudze\NrbdvsRedis\Services\Logger;
use Kudze\NrbdvsRedis\Services\RedisConnector;

class ClearCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'clear',
            'Clears all redis server',
            $container
        );
    }

    public function run(string $params)
    {
        $this->getLogger()->println("Clearing all redis keys...");
        $this->getContainer()->get(RedisConnector::class)->getRedis()->flushAll();
    }
}