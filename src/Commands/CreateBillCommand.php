<?php

namespace Kudze\NrbdvsRedis\Commands;

use DI\Container;
use Kudze\NrbdvsRedis\Exceptions\ModelAlreadyExists;
use Kudze\NrbdvsRedis\Exceptions\ValidationException;
use Kudze\NrbdvsRedis\Models\Bill;
use Kudze\NrbdvsRedis\Models\Company;
use Kudze\NrbdvsRedis\Models\User;
use Kudze\NrbdvsRedis\Repositories\Repository;
use Kudze\NrbdvsRedis\Services\TablePrinter;

class CreateBillCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'cbill',
            'Creates a bill',
            $container
        );
    }

    public function run(string $params)
    {
        $repository = $this->getContainer()->get(Repository::class);
        $logger = $this->getLogger();
        $inputter = $this->getInputter();

        $companies = $repository->findAll(Company::class);
        $companyID = $inputter->askForCompanyID($companies);

        $users = $repository->findAll(User::class);
        $userEmail = $inputter->askForUserEmail($users);

        $amount = $inputter->askForPositiveFloat("Enter ammount:");

        $bill = new Bill();
        $bill->setCompanyId($companyID);
        $bill->setUserEmail($userEmail);
        $bill->setAmount($amount);

        try {
            $repository->insert($bill);

            $logger->println("Bill has been successfully created!");
        } catch(ModelAlreadyExists) {
            $logger->println("A bill already exists from this company to this user!");
            $logger->println("Use ubill to update the bill!");
        }
    }
}