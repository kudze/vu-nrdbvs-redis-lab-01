<?php

namespace Kudze\NrbdvsRedis\Commands;

use DI\Container;
use Kudze\NrbdvsRedis\Models\Bill;
use Kudze\NrbdvsRedis\Models\User;
use Kudze\NrbdvsRedis\Repositories\Repository;
use Kudze\NrbdvsRedis\Services\TablePrinter;

class ListAllBillsCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'bills',
            'Lists all bills',
            $container
        );
    }

    public function run(string $params)
    {
        $repository = $this->getContainer()->get(Repository::class);
        $logger = $this->getLogger();

        $bills = $repository->findAll(Bill::class);

        if (empty($bills)) {
            $logger->println("No bills created yet!");
            return;
        }

        $tablePrinter = $this->getContainer()->get(TablePrinter::class);

        $logger->println("All bills:");
        $tablePrinter->printBills($bills);
    }
}