<?php

namespace Kudze\NrbdvsRedis\Commands;

use DI\Container;
use Kudze\NrbdvsRedis\Models\Bill;
use Kudze\NrbdvsRedis\Models\User;
use Kudze\NrbdvsRedis\Repositories\Repository;
use Kudze\NrbdvsRedis\Repositories\UserRepository;
use Kudze\NrbdvsRedis\Services\Session;
use Kudze\NrbdvsRedis\Services\TablePrinter;

class ListMyBillsCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'mybills',
            'Lists your bills',
            $container
        );
    }

    public function run(string $params)
    {
        $logger = $this->getLogger();
        $session = $this->getContainer()->get(Session::class);

        if(!$session->isLoggedInToAnyUser())
        {
            $logger->println("You arent logged in to any user!");
            return;
        }

        $user = $session->getLoggedInUser();
        $repository = $this->getContainer()->get(UserRepository::class);
        $bills = $repository->getUserBills($user);

        if (empty($bills)) {
            $logger->println("No bills created yet!");
            return;
        }

        $logger->println("All bills:");
        $tablePrinter = $this->getContainer()->get(TablePrinter::class);
        $tablePrinter->printBills($bills);
    }
}