<?php

namespace Kudze\NrbdvsRedis\Commands;

use DI\Container;
use Kudze\NrbdvsRedis\Exceptions\ModelNotFoundException;
use Kudze\NrbdvsRedis\Models\User;
use Kudze\NrbdvsRedis\Repositories\Repository;
use Kudze\NrbdvsRedis\Services\Hasher;
use Kudze\NrbdvsRedis\Services\Session;

class LoginUserCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'login',
            'Log in an user',
            $container
        );
    }

    public function run(string $params)
    {
        $logger = $this->getLogger();
        $session = $this->getContainer()->get(Session::class); //<-- More like fake session.

        if($session->isLoggedInToAnyUser())
        {
            $logger->println("You are already logged in!");
            return;
        }

        $inputter = $this->getInputter();
        $repository = $this->getContainer()->get(Repository::class);
        $hasher = $this->getContainer()->get(Hasher::class);

        $email = $inputter->askForEmail();
        $password = $inputter->askForInputWithPrompt('Enter password:');

        try {
            $user = $repository->find(User::class, ['email' => $email]);

            if(!$hasher->verify($user->getPassword(), $password))
                throw new ModelNotFoundException(User::getTable(), 'password');

            $session->setLoggedInUser($user);
        } catch(ModelNotFoundException) {
            $logger->println("Invalid credentials");
            return;
        }

        $logger->println("Welcome, " . $user->getEmail());
    }
}