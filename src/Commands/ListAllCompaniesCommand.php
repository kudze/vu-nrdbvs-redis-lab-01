<?php

namespace Kudze\NrbdvsRedis\Commands;

use DI\Container;
use Kudze\NrbdvsRedis\Models\Company;
use Kudze\NrbdvsRedis\Repositories\Repository;
use Kudze\NrbdvsRedis\Services\TablePrinter;

class ListAllCompaniesCommand extends AbstractCommand
{
    public function __construct(Container $container)
    {
        parent::__construct(
            'companies',
            'Lists all companies',
            $container
        );
    }

    public function run(string $params)
    {
        $repository = $this->getContainer()->get(Repository::class);
        $logger = $this->getLogger();

        $companies = $repository->findAll(Company::class);

        if (empty($companies)) {
            $logger->println("No companies exist yet!");
            return;
        }

        $tablePrinter = $this->getContainer()->get(TablePrinter::class);
        $tablePrinter->printCompanies($companies);

        $autoIncrement = $repository->getAutoIncrement(Company::class);
        $logger->println("Auto increment value: " . $autoIncrement);
    }
}