<?php

namespace Kudze\NrbdvsRedis\Models;

class Company extends AbstractModel
{
    protected static string $table = "companies";
    protected static array $keyCollumns = ['id'];
    protected static array $foreignKeys = [];

    public function __construct()
    {
        parent::__construct([
            'id' => null,
            'title' => '',
        ]);
    }

    public function setId(int $id): void
    {
        $this->setDataCol('id', $id);
    }

    public function getId(): ?int
    {
        return $this->getDataCol('id');
    }

    public function setTitle(string $email): void
    {
        $this->setDataCol('title', $email);
    }

    public function getTitle(): string
    {
        return $this->getDataCol('title');
    }
}