<?php

namespace Kudze\NrbdvsRedis\Models;

class User extends AbstractModel
{
    protected static bool $supportsAutoIncrKey = false;
    protected static string $table = "users";
    protected static array $keyCollumns = ['email'];

    public function __construct()
    {
        parent::__construct([
            'email' => '',
            'first_name' => '',
            'last_name' => '',
            'password' => '',
            'balance' => 0
        ]);
    }

    public function setEmail(string $email): void
    {
        $this->setDataCol('email', $email);
    }

    public function getEmail(): string
    {
        return $this->getDataCol('email');
    }

    public function setFirstName(string $email): void
    {
        $this->setDataCol('first_name', $email);
    }

    public function getFirstName(): string
    {
        return $this->getDataCol('first_name');
    }

    public function setLastName(string $email): void
    {
        $this->setDataCol('last_name', $email);
    }

    public function getLastName(): string
    {
        return $this->getDataCol('last_name');
    }

    public function setPassword(string $password): void
    {
        $this->setDataCol('password', $password);
    }

    public function getPassword(): string
    {
        return $this->getDataCol('password');
    }

    public function getMyBillsPattern(): string
    {
        return Bill::getTable() . ':data:' . $this->getEmail() . ':*';
    }

    public function setBalance(float $balance)
    {
        $this->setDataCol('balance', $balance);
    }

    public function getBalance() :?float
    {
        return $this->getDataCol('balance');
    }
}