<?php

namespace Kudze\NrbdvsRedis\Models;

class Bill extends AbstractModel
{
    protected static bool $supportsAutoIncrKey = false;
    protected static string $table = "bills";
    protected static array $keyCollumns = ['user_email', 'company_id'];

    public function __construct()
    {
        parent::__construct([
            'company_id' => null,
            'user_email' => null,
            'amount' => null
        ]);
    }

    public function getUserEmail(): ?string
    {
        return $this->getDataCol('user_email');
    }

    public function setUserEmail(string $user_email): void
    {
        $this->setDataCol('user_email', $user_email);
    }

    public function getCompanyId(): ?int
    {
        return $this->getDataCol('company_id');
    }

    public function setCompanyId(int $company_id): void
    {
        $this->setDataCol('company_id', $company_id);
    }

    public function getAmount(): ?float
    {
        return $this->getDataCol('amount');
    }

    public function setAmount(float $amount): void
    {
        $this->setDataCol('amount', $amount);
    }
}