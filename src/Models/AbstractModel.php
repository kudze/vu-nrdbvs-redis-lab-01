<?php

namespace Kudze\NrbdvsRedis\Models;

use Kudze\NrbdvsRedis\Exceptions\ModelNotAutoIncrementable;
use Kudze\NrbdvsRedis\Exceptions\ModelKeyNotDefinedException;

abstract class AbstractModel
{
    protected static string $table = "";
    protected static bool $supportsAutoIncrKey = true;
    protected static array $keyCollumns = [];

    public static function getTable(): string
    {
        return static::$table;
    }

    public static function supportsAutoIncrementKey(): bool
    {
        return static::$supportsAutoIncrKey;
    }

    public static function getDataKeysPattern(): string
    {
        return static::getTable() . ':data:*';
    }

    public static function getAutoIncrementKey(): string
    {
        if(!static::supportsAutoIncrementKey())
            throw new ModelNotAutoIncrementable(static::$table);

        return static::getTable() . ":auto-increment";
    }

    /**
     * Returns model key from data.
     *
     * @param array $data
     * @return string
     * @throws ModelKeyNotDefinedException
     */
    public static function getModelKeyFromData(array $data)
    {
        $table = static::getTable();
        $result = static::getTable() . ':data';

        foreach (static::getKeyColumns() as $column) {
            if (!array_key_exists($column, $data) || $data[$column] === null)
                throw new ModelKeyNotDefinedException($table, $column);

            $result .= ':' . $data[$column];
        }

        return $result;
    }

    public static function getKeyColumns(): array
    {
        return static::$keyCollumns;
    }

    private array $data;
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     * @throws ModelKeyNotDefinedException
     */
    public function getKey(): string
    {
        return self::getModelKeyFromData($this->data);
    }

    public function getCurrentIDValue(): int
    {
        if(!static::supportsAutoIncrementKey())
            throw new ModelNotAutoIncrementable(static::$table);

        return $this->data[static::getKeyColumns()[0]];
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function setDataCol(string $key, mixed $data): void
    {
        $this->data[$key] = $data;
    }

    public function getDataCol(string $key): mixed
    {
        return $this->data[$key];
    }
}