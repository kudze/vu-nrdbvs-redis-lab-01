<?php

require __DIR__ . '/vendor/autoload.php';

use Kudze\NrbdvsRedis\Commands\CommandManager;
use Kudze\NrbdvsRedis\Commands\RegisterUserCommand;
use Kudze\NrbdvsRedis\Commands\LoginUserCommand;
use Kudze\NrbdvsRedis\Commands\LogoutUserCommand;
use Kudze\NrbdvsRedis\Commands\ExitCommand;
use Kudze\NrbdvsRedis\Commands\ListAllUsersCommand;
use Kudze\NrbdvsRedis\Commands\CreateCompanyCommand;
use Kudze\NrbdvsRedis\Commands\ListAllCompaniesCommand;
use Kudze\NrbdvsRedis\Commands\ListAllBillsCommand;
use Kudze\NrbdvsRedis\Commands\ListMyBillsCommand;
use Kudze\NrbdvsRedis\Commands\PayMyBillsCommand;
use Kudze\NrbdvsRedis\Commands\CreateBillCommand;
use Kudze\NrbdvsRedis\Commands\ClearCommand;
use Kudze\NrbdvsRedis\Commands\HelpCommand;
use Kudze\NrbdvsRedis\Services\Logger;
use Kudze\NrbdvsRedis\Services\Inputter;
use Kudze\NrbdvsRedis\Exceptions\InvalidCommandException;

$container = new DI\Container();

//Lets register all commands we need.
$cmdManager = $container->get(CommandManager::class);
$cmdManager->registerCommand($container->get(RegisterUserCommand::class));
$cmdManager->registerCommand($container->get(LoginUserCommand::class));
$cmdManager->registerCommand($container->get(LogoutUserCommand::class));
$cmdManager->registerCommand($container->get(ListAllUsersCommand::class));
$cmdManager->registerCommand($container->get(ExitCommand::class));
$cmdManager->registerCommand($container->get(ClearCommand::class));
$cmdManager->registerCommand($container->get(CreateCompanyCommand::class));
$cmdManager->registerCommand($container->get(ListAllCompaniesCommand::class));
$cmdManager->registerCommand($container->get(HelpCommand::class));
$cmdManager->registerCommand($container->get(CreateBillCommand::class));
$cmdManager->registerCommand($container->get(ListAllBillsCommand::class));
$cmdManager->registerCommand($container->get(ListMyBillsCommand::class));
$cmdManager->registerCommand($container->get(PayMyBillsCommand::class));


$inputter = $container->get(Inputter::class);
$logger = $container->get(Logger::class);
$logger->println("Hello to interactive tax collector!");
$logger->println("Write help if you need any help with the tool.");
$logger->println("* Have fun! :)");
$logger->println("* Credits: Karolis Kraujelis");
$logger->println();

while(true) {
    $input = $inputter->askForInput();

    try {
        $cmdManager->parseInput($input);
    } catch(InvalidCommandException $e) {
        $logger->println($e->getCommand() . ' command doesnt exist!');
    }
}


